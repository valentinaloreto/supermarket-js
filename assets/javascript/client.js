function Client(product){
  this.product = product;
  this.found = false;
}

Client.prototype.render = function () {
  let client = document.createElement('div');
  client.classList.add('client');
  let shelfCount = document.querySelectorAll('.shelf').length;
  for (let i = 0; i < shelfCount; i++ ) {
    let $clientSpace = document.createElement('div');
    $clientSpace.classList.add('col-md-3', 'col-sm-6', 'standing-point');
    document.querySelector('.client-catwalk').append($clientSpace)
  }
  document.querySelector('.standing-point').prepend(client);
}


Client.prototype.move = function() {
  let client = document.querySelector('.client');
  let standingPoints = document.querySelectorAll('.standing-point');
  standingPoints[ Math.floor(Math.random() * standingPoints.length) ].prepend(client);
}

Client.prototype.checkForProduct = function() {
  let client = document.querySelector('.client').parentElement;
  let standing = document.querySelector('.client').parentNode.parentNode;
  //standing.indexOf(client);
  console.log(client);
  console.log( standing );
}
